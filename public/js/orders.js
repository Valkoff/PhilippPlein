$('#findOrder').submit(function (e) {
    e.preventDefault();
    findOrder(this);
});
$('#searchOrder').submit(function (e) {
    e.preventDefault();
    searchOrder(this);
});




function findOrder(form) {
    const baseUrl  =  '/api/orders/';
    let orderId = form.orderId.value;
    let url =  baseUrl + orderId;

    $.ajax({
        type: 'GET',
        url: url,
        success: showData
    });
    return true;
}

function searchOrder(form) {
    let url =  '/api/orders_search';
    let postedData = {
        count: form.count.value,
        startDate: form.startDate.value.replace('-', '').replace('-', ''),
        endDate: form.endDate.value.replace('-', '').replace('-', ''),
    };

    $.ajax({
        type: 'POST',
        url: url,
        data: postedData,
        success: showData
    });
    return true;
}

function showData(result) {
    let output = $('#output');
    output.html(result);
}