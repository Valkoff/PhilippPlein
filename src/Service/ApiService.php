<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use JsonException;

class ApiService
{

    private const ENDPOINT_BASE_URI = 'http://phillipplein/remote/api/';
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Client
     */
    private $client;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->client = new Client(['base_uri' => self::ENDPOINT_BASE_URI]);
    }

    /**
     * @return array
     */
    public function getRemoteData(): ?array
    {
        try {
            $response = $this->client->get('/remote-orders');
            return json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
        } catch (ClientException $e) {
            throw new Error('Data not found');
        } catch (JsonException $e) {
            return [];
        }

    }

    /**
     * @param  array  $data
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function storeData(array $data): bool
    {
        $orders = new ArrayCollection();
        $products = new ArrayCollection();
        foreach ($data as $orderItem) {
            $order = new Order();
            $order->setOrderId($orderItem->orderId);
            $order->setPhone($orderItem->phone);
            $order->setShippingStatus($orderItem->shipping_status);
            $order->setShippingPrice($orderItem->shipping_price);
            $order->setShippingPaymentStatus($orderItem->shipping_payment_status);
            $order->setPaymentStatus($orderItem->payment_status);
            $orders[$order->getOrderId()] = $order;

            foreach ($orderItem->orderItems as $orderedItem) {
                $product = new Product();
                $product->setOrder($order);
                $product->setBarcode($orderedItem->barcode ?? $orderedItem->item_sid);
                $product->setPrice($orderedItem->price);
                $product->setCost($orderedItem->cost);
                $product->setTaxPerc($orderedItem->tax_perc);
                $product->setTaxAmt($orderedItem->tax_amt);
                $product->setQuantity($orderedItem->quantity);
                $product->setTrackingNumber($orderedItem->tracking_number);
                $product->setCanceled($orderedItem->canceled ?? $orderedItem->Canceled);
                $product->setShippedStatusSku($orderedItem->shipped_status_sku);
                $products[$product->getBarcode()] = $product;
            }
        }

        foreach ($orders as $order) {
            $this->entityManager->persist($order);
        }
        $this->entityManager->flush();

        foreach ($products as $product) {
            $this->entityManager->persist($product);
        }
        $this->entityManager->flush();

        return true;
    }

}