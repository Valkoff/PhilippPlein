<?php

namespace App\Command;

use App\Service\ApiService;
use App\Service\StoreApiData;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ApiGetDataCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'api:get-data';
    /**
     * @var ApiService
     */
    protected $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Retrieves data from REST Api');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $data = $this->apiService->getRemoteData();
        if (count($data) > 0) {
            try {
                $this->apiService->storeData($data);
            } catch (OptimisticLockException $e) {

            } catch (ORMException $e) {
            }
            $io->success('Tried to import '.count($data));
        }

        return 0;
    }
}
