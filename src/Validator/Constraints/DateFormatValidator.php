<?php

namespace App\Validator\Constraints;

use DateTime;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;


/**
 * @Annotation
 */
class DateFormatValidator extends ConstraintValidator
{
    /**
     * @param  mixed  $value
     * @param  Constraint  $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ( ! $constraint instanceof DateFormat) {
            throw new UnexpectedTypeException($constraint, DateFormat::class);
        }

        if ( ! is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }

        if ( ! DateTime::createFromFormat('Ymd', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}