<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateFormat extends Constraint
{
    /**
     * @var string
     */
    public string $message = 'The string "{{ string }}" must be a date with format yyyymmdd.';
}