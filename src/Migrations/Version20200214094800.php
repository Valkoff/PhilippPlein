<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200214094800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, order_id VARCHAR(100) NOT NULL, phone VARCHAR(40) DEFAULT NULL, shipping_status VARCHAR(20) DEFAULT NULL, shipping_price DOUBLE PRECISION DEFAULT NULL, shipping_payment_status VARCHAR(20) DEFAULT NULL, payment_status VARCHAR(20) DEFAULT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_F52993988D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, barcode VARCHAR(60) NOT NULL, price DOUBLE PRECISION NOT NULL, cost DOUBLE PRECISION NOT NULL, tax_perc DOUBLE PRECISION NOT NULL, tax_amt DOUBLE PRECISION NOT NULL, quantity INT NOT NULL, tracking_number VARCHAR(100) NOT NULL, canceled VARCHAR(1) NOT NULL, shipped_status_sku VARCHAR(20) NOT NULL, UNIQUE INDEX UNIQ_D34A04AD97AE0266 (barcode), INDEX IDX_D34A04AD8D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD8D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD8D9F6D38');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE product');
    }
}
