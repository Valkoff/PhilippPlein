<?php

namespace App\Controller;

use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{

    /**
     * @return JsonResponse
     */
    public function allData(): JsonResponse
    {
        $string = '[
        {
            "orderItems": [
                {
                    "barcode": "4062345021658",
                    "price": 24300,
                    "cost": 24300,
                    "tax_perc": 20,
                    "tax_amt": 4050,
                    "quantity": 1,
                    "tracking_number": "1Z05V36Y7951053268",
                    "canceled": "N",
                    "shipped_status_sku": "not sent"
                },
                {
                    "item_sid": "4062345067851",
                    "price": 37800,
                    "cost": 37800,
                    "tax_perc": 20,
                    "tax_amt": 63000,
                    "quantity": 1,
                    "tracking_number": "1Z05V36Y7951053268",
                    "Canceled": "N",
                    "shipped_status_sku": "not sent"
                }
            ],
            "orderId": "PP0400104913",
            "phone": "+79620230303",
            "shipping_status": "not sent",
            "shipping_price": 1000,
            "shipping_payment_status": "paid",
            "payment_status": "paid"
        },
        {
            "orderItems": [
                {
                    "barcode": "4062345021658",
                    "price": 24300,
                    "cost": 24300,
                    "tax_perc": 20,
                    "tax_amt": 4050,
                    "quantity": 1,
                    "tracking_number": "1Z05V36Y7951053268",
                    "canceled": "N",
                    "shipped_status_sku": "not sent"
                },
                {
                    "item_sid": "4062345067851",
                    "price": 37800,
                    "cost": 37800,
                    "tax_perc": 20,
                    "tax_amt": 63000,
                    "quantity": 1,
                    "tracking_number": "1Z05V36Y7951053268",
                    "Canceled": "N",
                    "shipped_status_sku": "not sent"
                }
            ],
            "orderId": "PP040023123",
            "phone": "+79620230303",
            "shipping_status": "not sent",
            "shipping_price": 1000,
            "shipping_payment_status": "not paid",
            "payment_status": "not paid"
            }
        ]';


        try {
            $json = json_decode($string, true, 512, JSON_THROW_ON_ERROR);
            return $this->json($json);
        } catch (JsonException $e) {
            return $this->json([]);
        }

    }

}