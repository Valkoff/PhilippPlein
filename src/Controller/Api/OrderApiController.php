<?php

namespace App\Controller\Api;

use App\Entity\Order;
use App\Validator\Constraints\DateFormat;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderApiController extends Controller
{

    public function __construct()
    {
    }

    /**
     * @param  Request  $request
     * @param  ValidatorInterface  $validator
     * @return Response
     */

    public function search(Request $request, ValidatorInterface $validator): Response
    {
        $parameters = $request->query->all();
        $count = $request->get('count');
        $dateFrom = $request->get('startDate');
        $dateTo = $request->get('endDate');

        $errors = [
            'count' => $validator->validate($count, [new GreaterThan(0), new LessThanOrEqual(20)]),
            'date_from' => $validator->validate($dateFrom, new DateFormat()),
            'date_to' => $validator->validate($dateTo, new DateFormat()),
        ];

        foreach ($errors as $field => $error) {
            if (count($error) > 0) {
                return new Response($field.': '.$error[0]->getMessage());
            }
        }

        $dateFrom = $dateFrom ? DateTime::createFromFormat('Ymd', $dateFrom) : null;
        $dateTo = $dateFrom ? DateTime::createFromFormat('Ymd', $dateTo) : null;

        $data = $this->getDoctrine()->getRepository(Order::class)
            ->search($count, $dateFrom, $dateTo);

        return $this->render('order/table.html.twig', ['orders' => $data]);
    }

    /**
     * @param  string  $orderId
     * @return Response
     */
    public function find(string $orderId): Response
    {
        $data = $this->getDoctrine()->getRepository(Order::class)
            ->findBy(['orderId' => $orderId]);
        return $this->render('order/table.html.twig', ['orders' => $data]);
    }
}